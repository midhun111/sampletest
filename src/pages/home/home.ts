import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@IonicPage()

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public afDB: AngularFireDatabase) {

  }

  ngOnInit(){
    this.afDB.list('users').snapshotChanges().subscribe(data=>{
      console.log("DATA : ",data)
    })
  }

}
