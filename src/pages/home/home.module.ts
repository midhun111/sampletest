import { NgModule,ErrorHandler } from '@angular/core';
import { IonicPageModule,IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HomePage } from './home';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyDVzCZCAf3iyuLIHC1tLQl7f9i8QVUvJ5k",
  authDomain: "tineri-chat-a51ba.firebaseapp.com",
  databaseURL: "https://tineri-chat-a51ba.firebaseio.com",
  projectId: "tineri-chat-a51ba",
  storageBucket: "tineri-chat-a51ba.appspot.com",
  messagingSenderId: "53492838466"
};
@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class HomePageModule {}
